import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  act: boolean = true; 
  constructor() { }

  ngOnInit(): void {
    if(!window.location.href.includes("transactions") ){
      this.act = true;
    }
    else{
      this.act = false
    }
  }

  transaction(): void {
    if(this.act){
      this.act=false;
    }
  }
  customer(): void {
     if(this.act == false){
      this.act=true;
     }
  } 

}

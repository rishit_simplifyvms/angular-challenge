import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import {CustomerService} from '../../Service/customer.service';
import {TransactionService} from '../../../Transaction/Service/transaction.service';

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.css']
})
export class CustomerListComponent implements OnInit {
  users: any[] = [];
  private isSingleClick: boolean;
  constructor( private customerService: CustomerService, private transactionService: TransactionService) { }

  ngOnInit(): void {
    const data = new Observable((observer:any) => {
      fetch('http://localhost:3000/customers')
        .then(response => response.json()) 
        .then(data => {
          observer.next(data);
          observer.complete();
        })
        .catch(err => observer.error(err));
    });
    data.subscribe((data:any) => this.users = data);
  }

  detailsRight(customer: any): void {
    this.isSingleClick = true;
    setTimeout(()=>{
      if(this.isSingleClick){
        this.customerService.sendCustomerData(customer);
      }
    },250)
  }

  transactionTimeDetail(customer: any): void {
    this.isSingleClick = false;
    this.transactionService.updateDate(Date.now(), customer);
  }

}

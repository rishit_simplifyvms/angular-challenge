import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../../Service/customer.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-customer-detail',
  templateUrl: './customer-detail.component.html',
  styleUrls: ['./customer-detail.component.css']
})
export class CustomerDetailComponent implements OnInit {
  subscription: Subscription;
  customerData: any;
  act: boolean = false;
  constructor(private customerService: CustomerService) { }
  
  ngOnInit(): void {
      this.subscription = this.customerService.getCustomer().subscribe((customer) => {
        if (customer) {
          this.customerData = customer.customerData;
          this.act = customer.active;
        } 
      });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}

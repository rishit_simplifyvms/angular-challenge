import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  private subject = new Subject<any>();
  constructor() { }
  sendCustomerData(customers: any) {
    this.subject.next({ customerData: customers, active: true });
  }

  getCustomer(): Observable<any> {
    return this.subject.asObservable();
  }
}

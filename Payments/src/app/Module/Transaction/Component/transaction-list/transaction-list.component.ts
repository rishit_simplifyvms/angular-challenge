import { Component, OnInit } from '@angular/core';
import { observable, Observable } from 'rxjs';
import { TransactionService } from '../../Service/transaction.service';

@Component({
  selector: 'app-transaction-list',
  templateUrl: './transaction-list.component.html',
  styleUrls: ['./transaction-list.component.css']
})
export class TransactionListComponent implements OnInit {

  transactions: any; 
  constructor(private transactionService:  TransactionService) { }

  ngOnInit(): void {
    const data = new Observable((observer:any) => {
      fetch('http://localhost:3000/transactions')
        .then(response => response.json()) 
        .then(data => {
          observer.next(data);
          observer.complete();
        })
        .catch(err => observer.error(err));
    });
    data.subscribe((data:any) => {this.transactions = data;});
    
  }

  detailsRight(transaction: any): void {
      this.transactionService.sendTransactionData(transaction);
  }

}

import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { TransactionService } from '../../Service/transaction.service';

@Component({
  selector: 'app-transaction-component',
  templateUrl: './transaction-component.component.html',
  styleUrls: ['./transaction-component.component.css']
})
export class TransactionComponentComponent implements OnInit {

  act: boolean = false;
  subscription: Subscription;
  transactionData: any;
  constructor(private transactionService: TransactionService) { }

  ngOnInit(): void {
    this.subscription = this.transactionService.getTransaction().subscribe((transaction) => {
      if (transaction) {
        this.transactionData = transaction.transactionData;
        this.act = transaction.active;
      } 
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}

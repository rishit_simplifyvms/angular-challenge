import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { CustomerService } from '../../Customer/Service/customer.service';

@Injectable({
  providedIn: 'root'
})
export class TransactionService {

  private subject = new Subject<any>();

  constructor( private http: HttpClient) { }
  updateDate(today: number, customer: any) {
    this.http.patch(`http://localhost:3000/transactions/${customer.tid}`, {date: today}).subscribe((res)=> console.log(res));
  }

  sendTransactionData(transaction: any) {
    this.subject.next({ transactionData: transaction, active: true });
  }

  getTransaction(): Observable<any> {
    return this.subject.asObservable();
  }

}

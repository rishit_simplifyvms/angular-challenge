import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomerListComponent } from './Module/Customer/Component/customer-list/customer-list.component';
import { TransactionListComponent } from './Module/Transaction/Component/transaction-list/transaction-list.component';

const routes: Routes = [
  {
    path: '', redirectTo: 'customers', pathMatch: 'full'
  },
  {
    path: 'customers', component: CustomerListComponent
  },
  {
    path: 'transactions', component: TransactionListComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

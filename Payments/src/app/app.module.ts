import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ClarityModule, ClrIconModule } from '@clr/angular';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MenuComponent } from './Module/Main/Component/menu/menu.component';
import { CustomerListComponent } from './Module/Customer/Component/customer-list/customer-list.component';
import { CustomerDetailComponent } from './Module/Customer/Component/customer-detail/customer-detail.component';
import { TransactionListComponent } from './Module/Transaction/Component/transaction-list/transaction-list.component';
import { TransactionComponentComponent } from './Module/Transaction/Component/transaction-component/transaction-component.component';
import {MatGridListModule} from '@angular/material/grid-list';


@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    CustomerListComponent,
    CustomerDetailComponent,
    TransactionListComponent,
    TransactionComponentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ClarityModule,
    BrowserAnimationsModule,
    ClrIconModule,
    MatGridListModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

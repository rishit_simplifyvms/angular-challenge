import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EventsComponent } from './Component/events/events.component';
import { HomeComponent } from './Component/home/home.component';
import { TimelineComponent } from './Component/timeline/timeline.component';

const routes: Routes = [
  {path:'', redirectTo: 'home',pathMatch:'full'},
  {path:'home', component: HomeComponent},
  {path:'timeline', component: TimelineComponent},
  {path:'event', component: EventsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component } from '@angular/core';
import {TranslatesService } from './service/translates.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'User-profile-Event';
  act: any;
  lang: any = 'en';
  constructor( public translate: TranslateService, private translateService: TranslatesService) {
    if(window.location.href.includes("timeline")){
      this.act = "timeline";
    }
    else if (window.location.href.includes("event")){
      this.act = "event";
    }
    translate.addLangs(['en','hi']);
    translate.setDefaultLang('en');
    const browserLang = translate.getBrowserLang();
    translate.use(browserLang.match(/en|hi/) ? browserLang : 'en');
  }
  changeLang() {
    if(this.lang=='en'){
      this.lang = 'hi';
    }
    else{
      this.lang = 'en'
    }
    this.translate.use(this.lang);
    this.translateService.sendTranslateData(this.lang);
  }
}

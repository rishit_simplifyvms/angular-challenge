import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {
  events: any;
  constructor(public translate: TranslateService) { }

  ngOnInit(): void {
    const data = new Observable((observer:any) => {
      fetch('http://localhost:3000/events')
        .then(response => response.json()) 
        .then(data => {
          observer.next(data);
          observer.complete();
        })
        .catch(err => observer.error(err));
    });
    data.subscribe((data:any) => this.events = data);
  }
}

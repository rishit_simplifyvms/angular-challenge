import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.css']
})
export class TimelineComponent implements OnInit {
  timeline: any;
  constructor( public translate: TranslateService) { }

  ngOnInit(): void {
    const data = new Observable((observer:any) => {
      fetch('http://localhost:3000/timeline')
        .then(response => response.json()) 
        .then(data => {
          observer.next(data);
          observer.complete();
        })
        .catch(err => observer.error(err));
    });
    data.subscribe((data:any) => this.timeline = data);
  }

}

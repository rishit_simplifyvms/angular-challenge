import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TranslatesService {
  private subject = new Subject<any>();
  constructor() { }
  sendTranslateData(customers: any) {
    this.subject.next({ customerData: customers, active: true });
  }

  getTranslate(): Observable<any> {
    return this.subject.asObservable();
  }
}
